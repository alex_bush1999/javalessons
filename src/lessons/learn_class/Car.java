package lessons.learn_class;

public class Car {
    private String name;
    private int age;
    public static final int NUMBER;

    private static int count = 0;

    {
        System.out.println("Non-static block initialization");
    }

    static {
        NUMBER = 88;
        System.out.println("Static block initialization");
    }

    public Car(String name, int age) {
        System.out.println("Constructor");
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {//(this, age)
        int count = Car.count;
        this.age = age;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        Car.count = count;
    }
}

class TestCar {
    public static void main(String[] args) {
    /*    Car car1 = new Car("Car1", 12);

        car1.setAge(55);//this, 55

        Car.setCount(55);

        Car car2 = new Car("Car2", 22);

        car2.setAge(77);*/

        int[] array = {4, 7, 8, 9};

        for (int i = 0; i < array.length; i++) {
            int temp = array[i];
            System.out.println(temp);
        }

        for (int elem : array) {//foreach
            System.out.println(elem);
        }

//        System.out.println(Car.getCount());
    }
}
