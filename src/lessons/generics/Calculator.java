package lessons.generics;

public class Calculator<T> {
    public void print(T a, T b) {
        System.out.println("a: " + a + " b: " + b);
    }
}

class TestCalculator {
    public static void main(String[] args) {
        Calculator<Integer> calculatorInt = new Calculator<>();
        Calculator<Double> calculatorDouble = new Calculator<>();
    }
}
