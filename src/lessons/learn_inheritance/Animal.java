package lessons.learn_inheritance;

public abstract class Animal implements Jumpable, Runnable {
    private int age;
    private boolean isTail;

    public Animal(int age, boolean isTail) {
        this.age = age;
        this.isTail = isTail;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isTail() {
        return isTail;
    }

    public void setTail(boolean tail) {
        isTail = tail;
    }

    Object print() {
        System.out.println("Animal");
        return new Object();
    }

    protected abstract void getFile();

    public Thread print(String str) {
        System.out.println("Animal");
        return new Thread();
    }

    @Override
    public void jump() {
        System.out.println("I'm jumping");
    }

    @Override
    public void printInfo() {
        System.out.println(Jumpable.NUMBER);
    }

    @Override
    public void run() {

    }
}
