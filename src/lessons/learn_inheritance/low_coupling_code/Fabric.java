package lessons.learn_inheritance.low_coupling_code;

import java.util.ArrayList;
import java.util.List;

public class Fabric {
    private One one;
    private Two two;

    public Fabric(One one, Two two) {
        this.one = one;
        this.two = two;
    }

    public void actionOne() {
        one.print();
    }

    public void actionTwo() {
        two.print();
    }
}

class NewFabric {
    private Printable printable;

    public NewFabric(Printable printable) {
        this.printable = printable;
    }

    public void action() {
        printable.print();
    }
}

interface Printable {
    void print();
}

class One implements Printable {

    public void print() {
        System.out.println("One");
    }
}

class Two implements Printable {

    public void print() {
        System.out.println("Two");
    }
}

class Three implements Printable {

    public void print() {
        System.out.println("Three");
    }
}


class TestLowCode {
    public static void main(String[] args) {
        One one = new One();
        Two two = new Two();
        Three three = new Three();

        NewFabric newFabric = new NewFabric(three);
    }
}
