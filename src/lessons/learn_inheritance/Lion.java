package lessons.learn_inheritance;

import java.io.File;

public class Lion extends Animal {
    private boolean isLines;

    public Lion(int age, boolean isTail, boolean isLines) {
        super(age, isTail);
        this.isLines = isLines;
    }

    public boolean isLines() {
        return isLines;
    }

    public void setLines(boolean lines) {
        isLines = lines;
    }

    @Override
    protected File print() {
        System.out.println("Lion");
        return null;
    }

    public Thread print(String str, int age) {
        System.out.println("Animal");
        return new Thread();
    }

    @Override
    public void jump() {

    }

    @Override
    public void getFile() {

    }

    @Override
    public void run() {

    }
}

class TestInheritance {
    public static void main(String[] args) {
        Object lion = new Lion(12, true, true);
//        lion.print();
//        Jumpable animal = new Lion();
//        Runnable animal = new Lion();
    }

}


/*
* private - visibility only inside class
* package-private(default) - visibility only inside package
* protected - visibility only inside package + childs
* public - full visibility
* */
