package lessons.learn_inheritance.learn_polimorphysm;

public class Furniture {
    public void print() {
        System.out.println("Furniture");
    }
}

class Sofa extends Furniture {
    @Override
    public void print() {
        System.out.println("Sofa");
    }
}

class Chair extends Furniture {
    @Override
    public void print() {
        System.out.println("Chair");
    }
}

class TestPol {
    public static void main(String[] args) {
        Furniture[] furnitures = new Furniture[3];

        Furniture furniture = new Furniture();

        Furniture sofa = new Sofa();//

//        int a = "";

        String s = new String("String");


        Furniture chair = new Chair();

        furnitures[0] = furniture;
        furnitures[1] = sofa;
        furnitures[2] = chair;


        for (Furniture furn : furnitures) {
            furn.print();
        }
    }
}
