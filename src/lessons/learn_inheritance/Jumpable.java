package lessons.learn_inheritance;

public interface Jumpable {

    public static final int NUMBER = 5;

    public static void printFile() {
        System.out.println("print info");
    }

    public abstract void jump();

    default void printInfo() {
        System.out.println("");
    }
}
