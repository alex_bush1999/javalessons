package lessons.wrapper;

public class Wrapper {
    public static void main(String[] args) {
        final int y = 0;

        int a = 10;//Integer
        Integer b = 5;//new Integer(5)


        b = a;//new Integer(a) - autoboxing - b = new Integer(a);

        a = b;//unboxing

        int compare = Integer.compare(5, 8);
        int i = Integer.parseInt("4");
    }
}
