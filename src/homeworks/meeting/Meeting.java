package homework.meeting.meeting;

public class Meeting {


    private Person[] persons;//nulls
    private int topIndex;


    public Meeting() {
        persons = new Person[5];//write logic for resizing
        topIndex = 0; // top element of persons--delete
    }

    /**
     * This method add new elemnts to array
     * @param person
     */
    public void addPerson(Person person) {
    if (topIndex== persons.length-1) {
            growArray();
        }
        persons[++topIndex] = person; //function add elements to array
        showSuitablePersons(person);
    }



    /**
     * The method show list suitable persons by age and gender
     *
     * @param person
     */

    public void showSuitablePersons(Person person) { // function show info by years
        for (Person p : persons) {
            if (p != null && person.getAge() == p.getAge() && !person.getGender().equals(p.getGender()) ) { //compares user`s year with years all users
                System.out.println(" " + p.getName() + " " + p.getSurName() + " " + p.getPatronymic() +
                        "\n City :" + p.getCity() + " \n Age: " + p.getAge() +
                        "\n Amount of Child:" + p.getAmountOfChild() +
                        "\n Gender:" + p.getGender());//move to method in Person class

            }

        }
        System.out.print("\n\n");
    }

    /**
     *
     This function takes an object and looks for suitable persons by gender
     * @param gender
     */

    public void showSuitable(Gender gender) { // function show info by years, rename
        for (Person p : persons) {
            if (p != null && !gender.equals(p.getGender())) { //compares user`s year with years all users
                System.out.println(" " + p.getName() + " " + p.getSurName() + " " + p.getPatronymic() +
                        "\n City :" + p.getCity() + " \n Age: " + p.getAge() +
                        "\n Amount of Child:" + p.getAmountOfChild() +
                        "\n Gender:" + p.getGender());//move to method in Person class

            }
        }
        System.out.print("\n\n");
    }

    /**
     * This function can search people by name and lastname
     * @param name
     * @param surname
     */

    public void showInfoByNameAndSurname(String name, String surname) { //function show info by name and surname
        for (Person p : persons) {//replace on foreach
            if (p != null && p.getName().equals(name) && p.getSurName().equals(surname)) { // compares information in array with information are entered from the keyboard
                System.out.println(" " + p.getName()
                        + " " + p.getSurName()
                        + " " + p.getPatronymic() +
                        "\n City :" + p.getCity() + " \n Age: " + p.getAge() +
                        "\n  Amount of Child:" + p.getAmountOfChild() + "\n Gender:" + p.getGender());
            }
        }

    }

    /**
     * This function can search for items by city, gender, age and amount of children
     * @param city
     * @param gender
     * @param old
     * @param amounOfChild
     */
    public void smartSearch(String city, Gender gender, int old, int amounOfChild) { // function searches  information that  user entered
        for (Person p : persons) {

            int age = p.getAge();
            String tempCity = p.getCity();
            if ((p != null && p.getGender().equals(gender)) && amounOfChild == p.getAmountOfChild()
                    && old == age && tempCity.equals(city)) { // compares information are entered by user
                System.out.println(" " + p.getName() +
                        " " + p.getSurName() +
                        " " + p.getPatronymic() +
                        "\n City :" + tempCity +
                        " \n Age: " + age +
                        "\n Amount of Child:" + p.getAmountOfChild() + "\n");

            }
        }
    }

    /**
     * This function increase array if amount of elemnts more than  we created
     */
    public void growArray() {
        Person[] newArray = new Person[persons.length * 2];
        System.arraycopy(persons, 0, newArray, 0, topIndex);
        persons = newArray;
    }

}

