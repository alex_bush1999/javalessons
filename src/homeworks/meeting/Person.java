package homework.meeting.meeting;


public class Person {
    private String login; // declare variables
    private String password;
    private String name;
    private String surName;
    private String patronymic;
    private int age;
    private String city;
    private Gender gender;
    private int amountOfChild;

    public Person(String login, String password, String name, String surName, String patronymic, int age, String city, Gender gender, int amountOfChild) {
        this.login = login;
        this.password = password;
        this.name = name;
        this.surName = surName;
        this.patronymic = patronymic;
        this.age = age;
        this.city = city;
        this.gender = gender;
        this.amountOfChild = amountOfChild;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getSurName() {
        return surName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public int getAge() {
        return age;
    }

    public String getCity() {
        return city;
    }

    public Gender getGender() {
        return gender;
    }

    public int getAmountOfChild() {
        return amountOfChild;
    }
}