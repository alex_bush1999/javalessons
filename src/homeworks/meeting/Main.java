package homework.meeting.meeting;

import java.util.Scanner;

public class Main {

    public static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {
        Meeting persons = new Meeting();

        boolean notExit = true;

        while (notExit) {
            System.out.println("1.Registration  \n2.Search by gender  \n3.Search by Name and Surname \n4.Smart Search");

            int act = Integer.parseInt(getInfoFromUser("Select act"));

            switch (act) {
                case 1:
                    //move to Main class
                    int old = Integer.parseInt(getInfoFromUser("Input your age"));

                    if (old < 18) {
                        break;
                    }

                    String login = getInfoFromUser("Input your Login");

                    String password = getInfoFromUser("Input your Password");

                    String name = getInfoFromUser("Input your Name");

                    String surname = getInfoFromUser("Input your Surname");

                    String patronymic = getInfoFromUser("Input your Patronymic ");

                    String city = getInfoFromUser("Input your city ");

                    int amountOfChild = Integer.parseInt(getInfoFromUser("Input amount of your Children"));

                    int g = Integer.parseInt(getInfoFromUser("Input your gender \n1.MAN \n2.WOMAN"));

                    Person person = new Person(login, password, name, surname,
                                                patronymic, old, city, gender(g), amountOfChild);

                    persons.addPerson(person); // after you enter information we add it to array

                    break;
                case 2:
                    int gen = Integer.parseInt(getInfoFromUser("Input your gender \n1.MAN \n2.WOMAN"));

                    persons.showSuitable(gender(gen));
                    break;

                case 3:

                    String nameForSearch = getInfoFromUser("input your name");
                    String surnameForSearch = getInfoFromUser("Input your surname");
                    persons.showInfoByNameAndSurname(nameForSearch, surnameForSearch);
                    break;
                case 4:
                    int g2 = Integer.parseInt(getInfoFromUser("Input your gender \n1.MAN \n2.WOMAN"));
                    String city2 = getInfoFromUser("Input your city ");
                    int old2 = Integer.parseInt(getInfoFromUser("Input your age"));
                    int amountOfChild1 = Integer.parseInt(getInfoFromUser("Input amount of your Children"));
                    persons.smartSearch(city2, gender(g2), amountOfChild1, old2);
                case 0:
                    notExit = false;
                    break;
            }
        }
    }

    private static String getInfoFromUser(String message) {
        System.out.println(message);
        return SCANNER.next();
    }

    private static Gender gender(int g) {
        if (g == 1) {
            return Gender.MAN;
        }
        if (g == 2) {
            return Gender.WOMAN;
        }
        return null;
    }


}
