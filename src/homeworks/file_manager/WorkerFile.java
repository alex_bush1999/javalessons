package homework.meeting.file_manager;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class WorkerFile {


    void createDir(String nameOfDirectory) throws IOException {

        Files.createDirectories(Paths.get(nameOfDirectory));

    }

    void createTxtFile(String nameOfTxt) throws IOException {

        Files.createFile(Paths.get(nameOfTxt + ".txt"));
    }

    void deleteFile(String PathToFile) throws IOException {
        Files.deleteIfExists(Paths.get(PathToFile));
    }

    public void copy(String source, String dest) throws IOException {
        File mySource = new File(source);
        File myDest = new File(source);

        if (!myDest.isFile())
            Files.copy(mySource.toPath(), myDest.toPath());
    }

    Path renameFileAndDirectory(String oldName, String newNameString) throws IOException {
        Path p1 = Paths.get(oldName);
        return Files.move(p1, p1.resolveSibling(newNameString));
    }

    public void readContentOfDirectory(String PathToFile) throws IOException {
        Files.list(Paths.get(PathToFile))
                .forEach(System.out::println);
    }

}

class Text2Pdf {

    public static final String TEXT
            = "resources/text/jekyll_hyde.txt";
    public static final String DEST
            = "results/javaone/edition16/text2pdf.pdf";

    public static void main(String[] args)
            throws DocumentException, IOException {
        File file = new File(DEST);
        file.getParentFile().mkdirs();
        new Text2Pdf().createPdf(DEST);
    }

    public void createPdf(String dest)
            throws DocumentException, IOException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(dest));
        document.open();
        BufferedReader br = new BufferedReader(new FileReader(TEXT));
        String line;
        Paragraph p;
        Font normal = new Font(Font.FontFamily.TIMES_ROMAN, 12);
        Font bold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
        boolean title = true;
        while ((line = br.readLine()) != null) {
            p = new Paragraph(line, title ? bold : normal);
            p.setAlignment(Element.ALIGN_JUSTIFIED);
            title = line.isEmpty();
            document.add(p);
        }
        document.close();
    }
}
