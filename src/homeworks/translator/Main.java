package homework.meeting.translator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        Translator t = new Translator();

        t.addNewEnglishWord("Привет", "Hello");
        t.addNewEnglishWord("ты", "you");
        t.addNewEnglishWord("хороший", "good");
        t.addNewEnglishWord("человек", "man");
        t.addUkUaWord("Привіт", "Hello");
        t.addUkUaWord("ти", "you");
        t.addUkUaWord("людина", "man");
        t.addUaRus("Привіт", "Привет");
        t.addUaRus("Автомобіль", "Машина");


        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        //t.translator();

        System.out.print("1.Добавить Английское слово " +
                " \n2.Добавить Украинское слово" +
                "\n3.Английское слово с украинским переводом " +
                "\n4.Перевести \n"
        );
        int key = Integer.parseInt(br.readLine());
        switch (key) {
            case 1:
                String word1 = br.readLine();
                String word2 = br.readLine();
                t.addNewEnglishWord(word1, word2);
                break;
            case 2:
                String word3 = br.readLine();
                String word4 = br.readLine();
                t.addUaRus(word3, word4);
                break;
            case 3:
                String word5 = br.readLine();
                String word6 = br.readLine();
                t.addUkUaWord(word5, word6);
                break;
            case 4:
                t.translator();
        }
    }
}
