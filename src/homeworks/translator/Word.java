package homework.meeting.translator;

public class Word {


    String myWord;
    public Word(String myWord) {
        this.myWord = myWord;
    }

    public String getMyWord() {
        return myWord;
    }

    public void setMyWord(String myWord) {
        this.myWord = myWord;
    }
}
