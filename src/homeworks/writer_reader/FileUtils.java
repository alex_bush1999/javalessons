package homework.meeting.writer_reader;

import java.nio.file.Path;
import java.nio.file.Paths;

public class FileUtils {
    public static final String NAME_OF_FILE = "Poems.txt";
    public static final Path PATH = Paths.get(NAME_OF_FILE);

    public static final String TEXT_ADD = "I'm ready for writing to file ";

    public static final String REPLACEMENT_TEXT = "I'm from file: ";

}
