package homework.meeting.writer_reader.writer;

import java.io.IOException;

public interface Writable {
    public abstract void write() throws IOException;
}
