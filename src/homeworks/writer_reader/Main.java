package homework.meeting.writer_reader;
import homework.meeting.writer_reader.reader.ConcreteReader;
import homework.meeting.writer_reader.writer.ConcreteWriter;

import java.io.IOException;
import java.util.Collections;

public class Main {
    public static void main(String[] args) throws IOException {
        ConcreteWriter writer = new ConcreteWriter();
        ConcreteReader reader = new ConcreteReader();

        writer.write();
        System.out.println(reader.read());

        System.out.print("\n");

    }
}
