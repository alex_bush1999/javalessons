package homework.meeting.writer_reader.reader;

import java.io.IOException;

public interface Readable {
    String read() throws IOException;
}
