package homework.meeting.writer_reader.reader;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static homework.meeting.writer_reader.FileUtils.*;

public abstract class Reader implements Readable {

    /*public void modifyText() throws IOException {

        Charset charset = StandardCharsets.UTF_8;
        Path path = Paths.get(fileName);
        Files.write(path,
                new String(Files.readAllBytes(path), charset).replace(search, replace)
                        .getBytes(charset));
    }*/

    public String modifyText(String text) throws IOException {

       return text.replace(TEXT_ADD, REPLACEMENT_TEXT);
    }
}
