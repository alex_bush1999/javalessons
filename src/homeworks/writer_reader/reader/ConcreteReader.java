package homework.meeting.writer_reader.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import static homework.meeting.writer_reader.FileUtils.PATH;


public class ConcreteReader extends Reader {
    /*public String[] read() {
        List<String> list = new ArrayList<>();
        Scanner in = null;
        try {
            in = new Scanner(new File("Poems.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (in.hasNextLine())
            list.add(in.nextLine());
        String[] array = list.toArray(new String[0]);
        return array;
    }*/

    @Override
    public String read() throws IOException {

//        List<String> list = Files.readAllLines(PATH);

        BufferedReader reader = Files.newBufferedReader(PATH);

        String line = null;
//        String result = "";

        StringBuilder builder = new StringBuilder();

        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }

        return modifyText(builder.toString());
    }
}


